﻿#include <iostream>
using namespace std;

class Vector
{
private:
    double dlina = 0;
    double x;
    double y;
    double z;
public:
    Vector() : x(0), y(0), z(0)
    {}
    Vector(double _x, double _y, double _z): x(_x), y(_y), z(_z)
    {}
    void Show()
    {
        dlina = abs(sqrt(x * x + y * y + z * z));
        cout << "Dlina ="<< " "<< dlina;
    }
};

int main()
{
    Vector v(0, 4, 3);
    v.Show();
    return 0;
}

